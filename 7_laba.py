# Обращаемся к библиотеке
import math

while True:
    mass_f = []
    xmin, xmax, a, x = 0, 0, 0, 0

    while True:
        try:  # Ввод переменных
            a = float(input('Введите значение a:'))
            xmin = int(input("Введите мин значение x:"))
            xmax = int(input("Введите макс значение x:"))
            if xmin >= xmax:
                print('Неправильно введеный границы х')
            else:
                break
        except ValueError:
            print('Ошибка: неправильный формат')

    while True:  # Количество шагов
        try:
            steps = int(input('Кол-во шагов: '))
            if math.isclose(steps, 0.0) or steps < 0 or (xmax - xmin) < steps:
                print('Ошибка: введите еще раз')
            else:
                size = (xmax - xmin) / steps
                break
        except ValueError:
            print('Ошибка: неправильный формат')

    x, step = xmin, 0

    # Решение уравнения G
    while step <= steps:
        g1 = 6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)
        g2 = 9 * a ** 2 + 30 * a * x + 16 * x ** 2
        if math.isclose(g2, 0.0):
            mass_f.append((x, None))
        else:
            g = g1 / g2
            mass_f.append((x, g))
        x += size
        step += 1

    # Запись результатов подсчета функции G в файл
    file = open('Result.txt', 'w')
    file.write('Функция G\n')
    for elem in mass_f:
        if elem[1] is None:
            file.write('x = {:.5f} Ошибка! Нет решения.'.format(elem[0]) + '\n')
        else:
            file.write('x = {0:.5f} || G = {1:.5f}'.format(elem[0], elem[1]) + '\n')
    file.write('\n')
    file.close()

    mass_f.clear()  # Очищение массива
    step = 0  # Обнуление переменной для работы цикла
    x = xmin  # Возвращение начального значения "x"

    while step <= steps:
        # Решение уравнения F
        f = math.sin(28 * a ** 2 - 27 * a * x + 5 * x ** 2)
        if f > 1 or f < -1:
            mass_f.append((x, None))
        else:
            f = math.sin(28 * a ** 2 - 27 * a * x + 5 * x ** 2)
            mass_f.append((x, f))
        x += size
        step += 1

    # Запись результатов подсчета функции F в файл
    file = open('Result.txt', 'a')
    file.write('Функция F\n')
    for elem in mass_f:
        if elem[1] is None:
            file.write('x = {:.5f} Ошибка! Нет решения.'.format(elem[0]) + '\n')
        else:
            file.write('x = {0:.5f} || F = {1:.5f}'.format(elem[0], elem[1]) + '\n')
    file.write('\n')
    file.close()

    mass_f.clear()  # Очищение массива
    step = 0  # Обнуление переменной для работы цикла
    x = xmin  # Возвращение начального значения "x"

    while step <= steps:
        # Решение уравнения Y
        y1 = (21 * a ** 2 + 73 * a * x + 10 * x ** 2 + 1)
        if y1 <= 0:
            mass_f.append((x, None))
        else:
            y = (math.log(y1)) / (math.log(2))
            mass_f.append((x, y))
        x += size
        step += 1

    # Запись результатов подсчета функции Y в файл
    file = open('Result.txt', 'a')
    file.write('Функция Y\n')
    for elem in mass_f:
        if elem[1] is None:
            file.write('x = {:.5f} Ошибка! Нет решения.'.format(elem[0]) + '\n')
        else:
            file.write('x = {0:.5f} || Y = {1:.5f}'.format(elem[0], elem[1]) + '\n')
    file.write('\n')
    file.close()

    # Вывод содержимого файла
    file = open('Result.txt', 'r')
    for line in file:
        print(line, end='')
    file.close()

    # Выход из цикла по требованию пользователя
    while True:
        try:
            end = int(input("Выйти из цикла? ДА[1]/НЕТ[2]: "))
            if end == 1:
                print('Выхожу из программы...')
                exit(0)
            elif end == 2:
                print("Заново производим цикл")
                break
            else:
                print("Ошибка: нет такого варианта выбора, выберите 0 или 1")
        except ValueError:
            print("Ошибка: неправильный формат")
